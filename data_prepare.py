# coding: utf-8

"""
Подготовка данных для обучения нейросети

Входная директория должна содержать папки с изображениями, разбитыми по классам. Программа использует номер
обрабатываемой папки как метку класса.

Последовательно проходит все вложенные директории, находящиеся в поданной на вход директорию.
Берет изображение, делает ресайз к заданному размеру, записывает в массив изображений.
Делает повороты каждого изображения и также записывает в массив изображений.
Каждому изображению массива изображений ставится в соответствие метка класса.

Массивы изображений записываются в словарь, содержащий изображения и их метки классов.

Словарь сериализуется в целевую директорию. В случае возникновения конфликта имен старый файл ПЕРЕЗАПИСЫВАЕТСЯ.

"""

import math
import os
import sys

import argparse as ap
import numpy as np

from itertools import zip_longest
from PIL import Image

if sys.version_info[0] == 2:
    import CPickle
else:
    import _pickle as CPickle


ALLOWED_FORMATS = ('.jpg', '.jpeg')

ELEMENTS_PER_BATCH = 2000

KEYS = ['data', 'labels']


def rotate_and_crop(image_path, rotation_step, img_size, box=None):
    """
    Возвращает массив всех повернутых на углы от 0 до 360 с заданным шагом изображений, приведенных к нужному размеру

    :param image_path:
    :param rotation_step:
    :param img_size: Кортеж (w, h), к которому необходимо привести размер полученного после всех трансформаций изображения
    :param box: Область, которую необходимо вырезать, tuple = x_left_top, y_left_top, x_right_bottom, y_right_bottom
    :return list: список изображений, представленных в виде np.array их пикселов

    """

    image = Image.open(image_path)

    # Если не передана область для выделения, отступаем по 90 пикселов с каждой стороны
    if box is None:
        box = 90, 90, image.size[1] - 90, image.size[1] - 90

    rotated_images = list()

    for angle in np.arange(0, 360, rotation_step):
        img = image.rotate(angle)
        img = img.crop(box).resize(img_size)

        rotated_images.append(np.array(img))

    return rotated_images


def split_to_batches_and_save(images, labels, serialize_path, elements_per_batch=ELEMENTS_PER_BATCH):
    """

    :param images:
    :param labels:
    :param serialize_path:
    :param elements_per_batch:
    :return:

    """

    indices = np.arange(0, len(labels))

    np.random.shuffle(indices)

    for batch_number in range(math.ceil(len(labels) / elements_per_batch)):
        batch_indices = indices[(batch_number * elements_per_batch):((batch_number + 1) * elements_per_batch)]

        data_to_store = dict(zip_longest(KEYS, [images[batch_indices], labels[batch_indices]]))

        path = os.path.join(serialize_path, 'batch{}'.format(batch_number))

        with open(path, mode='wb') as fp:
            CPickle.dump(data_to_store, fp)


def prepare_dataset(args):
    """
    Формирует из изображений, разделенных по классам разными папками, файл Python Pickle

    :param args: аргументы парсера
    :return None:

    """

    if not os.path.exists(args.in_data_path):
        raise ValueError('Файл с путем {} не существует'.format(args.in_data_path))
    elif not os.path.exists(args.out_data_path):
        raise ValueError('Файл с путем {} не существует'.format(args.out_data_path))

    images, class_labels = list(), list()

    current_class_label = 1

    for item in os.listdir(args.in_data_path):

        print('Обрабатываю папку {} ...'.format(item))

        try:
            for nested_item in os.listdir(os.path.join(args.in_data_path, item)):
                file_ext = os.path.splitext(nested_item)[1]

                if file_ext is None or file_ext.lower() not in ALLOWED_FORMATS:
                    continue

                # Если изображение повреждено, оно не обрабатывается

                try:
                    rotated = rotate_and_crop(os.path.join(args.in_data_path, item, nested_item),
                                              30, (args.image_width, args.image_height))
                except Exception:
                    continue

                images += rotated
                class_labels += [current_class_label] * len(rotated)

            current_class_label += 1

            print('Готово', end='\n\n')
        except Exception:
            print('Невозможно обработать {} как папку'.format(os.path.join(args.in_data_path, item)), end='\n\n')
            continue

    print('Обработка данных закончена', end='\n\n')

    print('Подготавливаю данные')

    images, class_labels = np.array(images), np.array(class_labels)

    print('Разделяю данные на батчи и сохраняю в директорию {}'.format(args.out_data_path))

    split_to_batches_and_save(images, class_labels, args.out_data_path)

    print('Готово. Файл с размеченным датасетом сохранен в {}'.format(args.out_data_path))


def unpickle_data(path):
    """

    :param path:
    :return:
    :raise ValueError:

    """

    if not os.path.exists(path):
        raise ValueError('Невозможно открыть файл {}'.format(path))

    try:
        unpickled = CPickle.load(open(path, mode='rb'))
    except UnicodeDecodeError:
        from pickle import _Unpickler

        unpickled = _Unpickler(open(path, mode='rb'))
        unpickled.encoding = 'latin1'
        unpickled = unpickled.load()
    except Exception as e:
        raise e

    return unpickled


def generate_batches_from_dir(path):
    """

    :param path:
    :return:

    """

    if not os.path.exists(path):
        raise ValueError('Невозможно открыть папку {}'.format(path))

    for item in os.listdir(path):
        if os.path.splitext(item)[1]:
            continue

        unpickled = unpickle_data(os.path.join(path, item))

        yield unpickled['data'], unpickled['labels']


def setup_parser():
    parser = ap.ArgumentParser();

    parser.add_argument('-i', '--in_data_path', type=str, default=os.path.join(os.getcwd(), 'data\\raw'),
                        help='Path to raw data')
    parser.add_argument('-o', '--out_data_path', type=str, default=os.getcwd(),
                        help='Path to save pickled file (if not specified, current directory used)')

    parser.add_argument('-iw', '--image_width', type=int, default=224, help='Width to resize image')
    parser.add_argument('-ih', '--image-height', type=int, default=224, help='Height to resize image')

    return parser

if __name__ == '__main__':
    try:
        prepare_dataset(setup_parser().parse_args())
    except Exception as e:
        print(e)
