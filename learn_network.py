
# coding: utf-8

# # Transfer learning

# In[1]:

import lasagne
import os
import pickle
import theano

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import theano.tensor as T

from data_prepare import unpickle_data, generate_batches_from_dir
from lasagne.objectives import aggregate
from nn_prepare import build_nn
from sklearn.cross_validation import train_test_split


# In[2]:

#get_ipython().magic('matplotlib inline')


# ## Используемые константы

# In[3]:

IMAGE_SHAPE = (-1, 3, 224, 224)

dataset_path = os.path.join(os.getcwd(), 'data', 'prepared')

model_weights_path = os.path.join(os.getcwd(), 'nn_weights', 'vgg_cnn_s.pkl')


# ## Подготовка нейросети

# ### Входные и выходные данные

# In[4]:

X_sym = T.tensor4('input_var')
y_sym = T.ivector('target_var')


# ### Построение нейросети и инициализация весов

# In[5]:

net = build_nn(X_sym)

from pickle import _Unpickler

unpickled = _Unpickler(open(model_weights_path, mode='rb'))
unpickled.encoding = 'latin1'
net_weights = unpickled.load()

lasagne.layers.set_all_param_values(net['fc8'], net_weights['values'])

nn = lasagne.layers.DenseLayer(net['drop7'], num_units=8,
                               nonlinearity=lasagne.nonlinearities.softmax)


# ### Функции обучения, валидации и предсказания

# In[6]:

y_pred = lasagne.layers.get_output(nn)
params = lasagne.layers.get_all_params(nn, trainable=True)

loss = aggregate(lasagne.objectives.categorical_crossentropy(y_pred, y_sym))

cost = T.grad(loss, params)

acc = T.mean(T.eq(T.argmax(y_pred, axis=1), y_sym), dtype=theano.config.floatX)

updates = lasagne.updates.nesterov_momentum(cost, params, learning_rate=0.0001,
                                            momentum=0.9)

train_f = theano.function([X_sym, y_sym], loss, updates=updates,
                          allow_input_downcast=True)

val_f = theano.function([X_sym, y_sym], [loss, acc],
                        allow_input_downcast=True)

pred_f = theano.function([X_sym], y_pred, allow_input_downcast=True)


# ## Обучение модели

# ### Вспомогательные функции

# In[7]:

def batches(iterable, N):
    chunk = list()

    for item in iterable:
        chunk.append(item)

        if len(chunk) == N:
            yield chunk
            chunk.clear()

    if chunk:
        yield chunk


# ### Обучение минибатчами

# In[8]:

BATCH_SIZE = 16


# In[9]:

def iterate_minibatches(X, y, batch_size, shuffle=True):
    if len(X) != len(y):
        raise ValueError('Количество объектов X должно быть равно количеству меток класса у')

    if shuffle:
        indices = np.arange(len(X))
        np.random.shuffle(indices)

    for start_idx in range(0, len(X) - batch_size - 1, batch_size):
        if shuffle:
            chunk = indices[start_idx:(start_idx + batch_size)]
        else:
            chunk = slice(start_idx, start_idx + batch_size)

        yield X[chunk], y[chunk]


# In[10]:

N_EPOCHS = 2


# In[ ]:

epoch_params, errors, accuracies = list(), list(), list()

for batch_num, batch in enumarete(generate_batches_from_dir(dataset_path)):
    if batch_num >= 4:
        break

    X, y = batch

    X = X.reshape(IMAGE_SHAPE)

    mean, std = np.mean(X, axis=0), np.std(X, axis=0).clip(min=1)

    X = (X - mean) / std

    X_train, y_train, X_val, y_val = train_test_split(X, y, test_size=0.2, random_state=42)

    for epoch in range(N_EPOCHS):
        ''' Train '''
        train_err, n_trained_batches = (0, ) * 2

        ''' Val '''
        val_err, val_acc, n_val_batches = (0, ) * 3

        for batch_n, batch in enumerate(generate_batches_from_dir(dataset_path)):
            if batch_n >= 5:
                break

            X, y = batch

            mean, std = np.mean(X, axis=0), np.std(X, axis=0).clip(min=1)

            X = (X - mean) / std

            X = X.reshape(IMAGE_SHAPE)

            X_train, y_train, X_val, y_val = train_test_split(X, y, test_size=0.2, random_state=42)

            for X, y in iterate_minibatches(X_train, y_train, BATCH_SIZE):
                train_err += train_f(X, y)

                n_trained_batches += 1

            for X, y in iterate_minibatches(X_val, y_val, BATCH_SIZE, shuffle=False):
                err, acc = val_f(X, y)

                val_err += err
                val_acc += acc

                n_val_batches += 1

        ''' Output '''

        train_err /= n_trained_batches

        val_err /= n_val_batches
        val_acc /= n_val_batches

        errors.append(val_err)
        accuracies.append(val_acc)

        epoch_params.append(lasagne.layers.get_all_param_values(cnn))

        print('Epoch {}, train_err = {}, val_err = {}, acc = {}'.format(epoch + 1,
                                                                        train_err,
                                                                        val_err,
                                                                        val_acc))


# ## Сериализация модели

# In[ ]:

max_acc_idx = np.argmax(accuracies)

if max_acc_idx is not None:
    best_model_params = epoch_params[max_acc_idx]

    with open('model.dat', mode='wb') as f:
        pickle.dump(best_model_params, f)


# ## Проверка модели

# In[ ]:

with open('model.dat', mode='rb') as f:
    params = pickle.load(f)

cnn, *other = compile_cnn()

lasagne.layers.set_all_param_values(cnn, params)

y_pred = list()

for X in X_test:
    y_pred.append(pred_f([X])[0])


# In[ ]:

cm = confusion_matrix(y_test, y_pred)

fig = plt.figure(figsize=(20, 20))
sns.heatmap(cm, annot=True, fmt='d', linewidth=0.5)
plt.show()
